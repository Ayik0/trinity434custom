-- well of eternity

-- los de reyna azshara tienen la vida mal
UPDATE creature_template SET Health_mod=5.00105299 WHERE entry=54589;
UPDATE creature_template SET Health_mod=5.00105299 WHERE entry=56579;
UPDATE creature_template SET Health_mod=11.0000489 WHERE entry=54883;
UPDATE creature_template SET Health_mod=11.0000489 WHERE entry=54882;
UPDATE creature_template SET Health_mod=11.0000489 WHERE entry=54884;

-- tira una spell cada 0,
-- Abyssal Doombringer
SET @ENTRY := 55510;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,2,0,100,30,0,2,95000,96000,11,103992,1,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Abyssal Doombringer"),
(@ENTRY,@SOURCETYPE,6,0,0,0,100,8,600000,600000,600000,600000,11,103992,1,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Berserk in 25 HC = 10 min");

-- cambiar el respawn de la mazmorra
UPDATE creature SET Spawntimesecs=86400 WHERE map=939;