-- http://es.wowhead.com/quest=11900
UPDATE quest_template SET RequiredNpcOrGo1=188100, RequiredNpcOrGo2=188101, RequiredNpcOrGo3=188102, RequiredNpcOrGo4=188103 WHERE id=11900;

-- http://es.wowhead.com/quest=11602
UPDATE quest_template SET RequiredNpcOrGo4=187655 WHERE id=11602;

-- http://es.wowhead.com/quest=24852/nuestra-tribu-prisionera
UPDATE quest_template SET RequiredNpcOrGo1=202112 WHERE id=24852;

-- http://es.wowhead.com/quest=11358
UPDATE creature_template SET faction_A=16, faction_H=16 WHERE entry=24381;
UPDATE creature_template SET Health_mod=4.85254178 WHERE entry=24381;

-- http://es.wowhead.com/quest=11247/arde-skorn-arde
UPDATE creature_ai_scripts SET action2_param1=43057 WHERE id=77770509;
UPDATE creature_ai_scripts SET action2_param1=43057 WHERE id=77770510;
UPDATE creature_ai_scripts SET action2_param1=43057 WHERE id=77770511;

-- http://es.wowhead.com/quest=11319/semillas-de-los-vigilantes-almanegra
UPDATE quest_template SET RequiredNpcOrGo1=23876 WHERE id=11319;

-- http://es.wowhead.com/quest=11314/las-hermanas-caidas
UPDATE quest_template SET RequiredNpcOrGo1=23678 WHERE id=11314;

-- http://es.wowhead.com/quest=11296/cautivos-de-el-bosque-hendido
SET @ENTRY := 24210;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,1,6,0,100,0,0,0,0,0,11,43289,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Riven Widow Cocoon - On Death - Summon Freed Winterhoof Longrunner (25%)"),
(@ENTRY,@SOURCETYPE,1,0,61,0,100,0,0,0,0,0,33,24211,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"Riven Widow Cocoon - On Death - Quest Credit");

-- http://es.wowhead.com/quest=11307/ensayo-de-campo
UPDATE creature_template SET AIName='SmartAI' WHERE entry=23564;
UPDATE quest_template SET RequiredNpcOrGo1=23564 WHERE id=11307;

-- http://es.wowhead.com/quest=11282/una-leccion-de-miedo
UPDATE quest_template SET RequiredNpcOrGo1=24161, RequiredNpcOrGo2=24016, RequiredNpcOrGo3=24162 WHERE id=11282;

-- http://es.wowhead.com/quest=11936/incubando-un-plan
UPDATE quest_template SET RequiredNpcOrGo1=188133 WHERE id=11936;
