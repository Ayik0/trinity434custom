-- FIXED PALADIN SPELL

-- COMUNION PALADIN RETRIBUCION

DELETE FROM spell_script_names WHERE spell_id in(465, 7294, 19746, 19891);
INSERT INTO `spell_script_names` VALUES (465, 'spell_pal_communion');
INSERT INTO `spell_script_names` VALUES (7294, 'spell_pal_communion');
INSERT INTO `spell_script_names` VALUES (19746, 'spell_pal_communion');
INSERT INTO `spell_script_names` VALUES (19891, 'spell_pal_communion');

-- ACUSAR PALADIN SAGRADO

DELETE FROM spell_proc_event WHERE entry in(31825 , 85510);
INSERT INTO `spell_proc_event` VALUES (31825, 0, 0, 0, 2, 0, 65536, 0, 0, 50, 0);
INSERT INTO `spell_proc_event` VALUES (85510, 0, 0, 0, 2, 0, 65536, 0, 0, 100, 0);

-- VELICIDAD DE LUZ PALADIN SAGRADO
DELETE FROM `spell_script_names` WHERE spell_id = 498 and ScriptName = 'spell_pal_divine_protection';
INSERT INTO `spell_script_names` VALUES (498, 'spell_pal_divine_protection');

-- DEFENSOR CANDENTE PALADIN PROTECCION 

DELETE FROM `spell_script_names` WHERE `spell_id`=31850;
INSERT INTO `spell_script_names` (`spell_id`,`ScriptName`) VALUES
(31850,'spell_pal_ardent_defender');