/*
 * Copyright (C) 2013-2014 Luis-Gomez <https://gitlab.com/u/Luis-Gomez>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"

class npc_promohorda : public CreatureScript 

{
public:
    npc_promohorda() : CreatureScript("npc_promohorda") { } 
 
    bool OnGossipHello(Player* player, Creature* creature) OVERRIDE 
    {
        player->ADD_GOSSIP_ITEM(7, "Escoge tu promocion: ", GOSSIP_SENDER_MAIN, 99);
		switch (player->getClass())
		{
				case CLASS_DRUID: player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Equilibrio", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+30); player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Feral", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+44); player->ADD_GOSSIP_ITEM(10, "Promocion de Druida Restauracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+45); break;
				case CLASS_SHAMAN: player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Elemental", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+31); player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Mejora", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+46); player->ADD_GOSSIP_ITEM(10, "Promocion de Chaman Restauracion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+47); break;
				case CLASS_PALADIN: player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Represion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+32); player->ADD_GOSSIP_ITEM(10, "Promocion de Paladin Sagrado", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+40); break;
				case CLASS_WARRIOR: player->ADD_GOSSIP_ITEM(10, "Promocion de Guerrero DPS", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+33); break;
				case CLASS_PRIEST: player->ADD_GOSSIP_ITEM(10, "Promocion de Sacerdote Sombras", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+34); player->ADD_GOSSIP_ITEM(10, "Promocion de Sacerdote Sagrado o Disciplina", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+42); break;
				case CLASS_DEATH_KNIGHT: player->ADD_GOSSIP_ITEM(10, "Promocion de DK Escarcha", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+35); player->ADD_GOSSIP_ITEM(10, "Promocion de DK Profano", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+49); break;
				case CLASS_ROGUE: player->ADD_GOSSIP_ITEM(10, "Promocion de Picaro", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+36); break;
				case CLASS_HUNTER: player->ADD_GOSSIP_ITEM(10, "Promocion de Cazador", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+37); break;
				case CLASS_MAGE: player->ADD_GOSSIP_ITEM(10, "Promocion de Mago", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+38); break;
				case CLASS_WARLOCK: player->ADD_GOSSIP_ITEM(10, "Promocion de Brujo", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+39); break;
		}
                player->SEND_GOSSIP_MENU(1, creature->GetGUID()); 
                return true;
    }
 
    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 actions) OVERRIDE
    {
	    
	if (player->getLevel() == 85) 
	{
            player->GetSession()->SendAreaTriggerMessage("No puedes recibir la promocion mas de una vez.");
	    player->CLOSE_GOSSIP_MENU();
	    return true;
        }
		
        if (player->getLevel() == 1) 
        {
            uint32 accountID = player->GetSession()->GetAccountId();
            QueryResult result = CharacterDatabase.PQuery("SELECT COUNT(`guid`) FROM `characters` WHERE `account`=%u", accountID);
            Field *fields = result->Fetch();
            uint32 personajes = fields[0].GetUInt32();

            if (personajes == 1)		
                    {
					        if (actions == 99)
						{
						    player->CLOSE_GOSSIP_MENU();
						    return false;
						}
						player->GetSession()->SendAreaTriggerMessage("Has recibido tu promocion satisfactoriamente, Enhorabuena.");
						 player->GiveLevel(85);
						player->SetMoney(200000000);
						switch(actions)
                              {								  
								  case GOSSIP_ACTION_INFO_DEF+30: // Druida Equilibrio
								  player->AddItem(70671, 1);
								  player->AddItem(70672, 1);
								  player->AddItem(70673, 1);
								  player->AddItem(70674, 1);
								  player->AddItem(70675, 1);
								  player->AddItem(70526, 1);
								  player->AddItem(70571, 1);
								  player->AddItem(70523, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70557, 1);
								  player->AddItem(70521, 1);
							      player->AddItem(70520, 1);
								  player->AddItem(61341, 1);
								  player->AddItem(70613, 1);
								  player->AddItem(61388, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+44: // Druida Feral
								  player->AddItem(70550, 1);
								  player->AddItem(70551, 1);
								  player->AddItem(70552, 1);
								  player->AddItem(70553, 1);
								  player->AddItem(70554, 1);
								  player->AddItem(70528, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70515, 1);
								  player->AddItem(70621, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70577, 1);
								  player->AddItem(70531, 1);
								  player->AddItem(70664, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(70637, 1);
								  player->AddItem(61343, 1);
								  player->AddItem(61390, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+45: // Druida Restauracion
								  player->AddItem(70580, 1);
								  player->AddItem(70581, 1);
								  player->AddItem(70582, 1);
								  player->AddItem(70583, 1);
								  player->AddItem(70584, 1);
								  player->AddItem(70526, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70622, 1);
								  player->AddItem(70564, 1);
								  player->AddItem(70572, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70556, 1);
								  player->AddItem(70522, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61342, 1);
								  player->AddItem(70524, 1);
								  player->AddItem(61389, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+31: // Chaman Elemental
								  player->AddItem(70597, 1);
								  player->AddItem(70598, 1);
								  player->AddItem(70599, 1);
								  player->AddItem(70600, 1);
								  player->AddItem(70601, 1);
								  player->AddItem(70512, 1);
								  player->AddItem(70621, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70665, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70557, 1);
								  player->AddItem(70521, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61341, 1);
								  player->AddItem(70640, 1);
								  player->AddItem(61388, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+46: // Chaman Mejora
								  player->AddItem(70590, 1);
								  player->AddItem(70591, 1);
								  player->AddItem(70592, 1);
								  player->AddItem(70593, 1);
								  player->AddItem(70594, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70641, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70669, 1);
								  player->AddItem(70577, 1);
								  player->AddItem(70596, 1);
								  player->AddItem(70531, 1);
								  player->AddItem(70638, 1);
								  player->AddItem(70637, 1);
								  player->AddItem(61324, 2);
								  player->AddItem(70613, 1);
								  player->AddItem(61390, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+47: // Chaman Restauracion
								  player->AddItem(70632, 1);
								  player->AddItem(70633, 1);
								  player->AddItem(70634, 1);
								  player->AddItem(70635, 1);
								  player->AddItem(70636, 1);
								  player->AddItem(70511, 1);
								  player->AddItem(70622, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70564, 1);
								  player->AddItem(70642, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70556, 1);
								  player->AddItem(70522, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61342, 1);
								  player->AddItem(70666, 1);
								  player->AddItem(61389, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+32: // Paladin Represion
								  player->AddItem(70648, 1);
								  player->AddItem(70649, 1);
								  player->AddItem(70650, 1);
								  player->AddItem(70651, 1);
								  player->AddItem(70652, 1);
								  player->AddItem(70514, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70668, 1);
								  player->AddItem(70543, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70573, 1);
								  player->AddItem(70539, 1);
								  player->AddItem(70579, 1);
								  player->AddItem(70654, 1);
								  player->AddItem(70653, 1);
								  player->AddItem(61346, 1);
								  player->AddItem(61391, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+40: // Paladin Healer 
								  player->AddItem(70615, 1);
								  player->AddItem(70616, 1);
								  player->AddItem(70617, 1);
								  player->AddItem(70618, 1);
								  player->AddItem(70619, 1);
								  player->AddItem(70530, 1);
								  player->AddItem(70540, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70622, 1);
								  player->AddItem(70564, 1);
								  player->AddItem(70576, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70556, 1);
								  player->AddItem(70522, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61338, 1);
								  player->AddItem(61361, 1);
								  player->AddItem(61389, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+34: // Sacerdote Sombras
								  player->AddItem(70643, 1);
								  player->AddItem(70644, 1);
								  player->AddItem(70645, 1);
								  player->AddItem(70646, 1);
								  player->AddItem(70647, 1);
								  player->AddItem(70621, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70545, 1);
								  player->AddItem(70660, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70549, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70557, 1);
								  player->AddItem(70521, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61341, 1);
								  player->AddItem(61350, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+42: // Sacerdote Sagrado
								  player->AddItem(70608, 1);
								  player->AddItem(70609, 1);
								  player->AddItem(70610, 1);
								  player->AddItem(70611, 1);
								  player->AddItem(70612, 1);
								  player->AddItem(70662, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70622, 1);
								  player->AddItem(70564, 1);
								  player->AddItem(70548, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70556, 1);
								  player->AddItem(70522, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61342, 1);
								  player->AddItem(70546, 1);
								  player->AddItem(61351, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+33: // Guerrero Dps
								  player->AddItem(70623, 1);
								  player->AddItem(70624, 1);
								  player->AddItem(70625, 1);
								  player->AddItem(70626, 1);
								  player->AddItem(70627, 1);
								  player->AddItem(70514, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70668, 1);
								  player->AddItem(70543, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70573, 1);
								  player->AddItem(70539, 1);
								  player->AddItem(70579, 1);
								  player->AddItem(70654, 1);
								  player->AddItem(70653, 1);
								  player->AddItem(61346, 2);
								  player->AddItem(61347, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
								  
								  case GOSSIP_ACTION_INFO_DEF+35: // DK Escarcha
								  player->AddItem(70558, 1);
								  player->AddItem(70559, 1);
								  player->AddItem(70560, 1);
								  player->AddItem(70561, 1);
								  player->AddItem(70562, 1);
								  player->AddItem(70514, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70668, 1);
								  player->AddItem(70543, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70573, 1);
								  player->AddItem(70539, 1);
								  player->AddItem(70579, 1);
								  player->AddItem(70654, 1);
								  player->AddItem(70653, 1);
								  player->AddItem(61345, 2);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+49: // DK Profano
								  player->AddItem(70558, 1);
								  player->AddItem(70559, 1);
								  player->AddItem(70560, 1);
								  player->AddItem(70561, 1);
								  player->AddItem(70562, 1);
								  player->AddItem(70514, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70668, 1);
								  player->AddItem(70543, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70573, 1);
								  player->AddItem(70539, 1);
								  player->AddItem(70579, 1);
								  player->AddItem(70654, 1);
								  player->AddItem(70653, 1);
								  player->AddItem(61346, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+36: // Picaro
								  player->AddItem(70585, 1);
								  player->AddItem(70586, 1);
								  player->AddItem(70587, 1);
								  player->AddItem(70588, 1);
								  player->AddItem(70589, 1);
								  player->AddItem(70528, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70638, 1);
								  player->AddItem(70515, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70577, 1);
								  player->AddItem(70531, 1);
								  player->AddItem(70664, 1);
								  player->AddItem(70637, 1);
								  player->AddItem(61327, 1);
								  player->AddItem(61328, 1);
								  player->AddItem(70613, 1);
								  player->AddItem(61347, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+37: // Cazador
								  player->AddItem(70533, 1);
								  player->AddItem(70534, 1);
								  player->AddItem(70535, 1);
								  player->AddItem(70536, 1);
								  player->AddItem(70537, 1);
								  player->AddItem(70638, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70641, 1);
								  player->AddItem(70669, 1);
								  player->AddItem(70577, 1);
								  player->AddItem(70596, 1);
								  player->AddItem(70531, 1);
								  player->AddItem(70637, 1);
								  player->AddItem(61340, 1);
								  player->AddItem(61355, 1);
								  player->AddItem(70613, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+38: // Mago
								  player->AddItem(70655, 1);
								  player->AddItem(70656, 1);
								  player->AddItem(70657, 1);
								  player->AddItem(70658, 1);
								  player->AddItem(70659, 1);
								  player->AddItem(70660, 1);
								  player->AddItem(70621, 1);
								  player->AddItem(70545, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70549, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70557, 1);
								  player->AddItem(70521, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61341, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(61350, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;

								  case GOSSIP_ACTION_INFO_DEF+39: // Brujo
								  player->AddItem(70566, 1);
								  player->AddItem(70567, 1);
								  player->AddItem(70568, 1);
								  player->AddItem(70569, 1);
								  player->AddItem(70570, 1);
								  player->AddItem(70660, 1);
								  player->AddItem(71665, 1);
								  player->AddItem(70621, 1);
								  player->AddItem(70545, 1);
								  player->AddItem(70563, 1);
								  player->AddItem(70549, 1);
								  player->AddItem(70578, 1);
								  player->AddItem(70557, 1);
								  player->AddItem(70521, 1);
								  player->AddItem(70520, 1);
								  player->AddItem(61341, 1);
								  player->AddItem(61350, 1);
								  player->CLOSE_GOSSIP_MENU();
								  break;
	                          }
                    }			
			
		    if (personajes > 1)
                    {
                        player->GetSession()->SendAreaTriggerMessage("No puedes recibir la promocion teniendo mas de un personaje.");
			player->CLOSE_GOSSIP_MENU();
			return true;
                    }
			
		    player->CLOSE_GOSSIP_MENU();
        }
        return true;
    }
};
 
void AddSC_npc_promohorda() 
{
    new npc_promohorda(); 
}