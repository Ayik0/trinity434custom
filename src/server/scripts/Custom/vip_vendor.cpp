#include "ScriptPCH.h"

class vip_vendor : public CreatureScript
{
public:
    vip_vendor() : CreatureScript("vip_vendor") { }

    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
		if(pPlayer->GetSession()->IsPremium())
		{
            pPlayer->GetSession()->SendListInventory(pCreature->GetGUID());
		}	
        else
		{
            pCreature->MonsterWhisper("Fuera de aqui, $N!", pPlayer->GetGUID());
		}	

        return true;
    }

    bool OnGossipSelect(Player *pPlayer, Creature *pCreature, uint32 sender, uint32 action)
    {
        if(!pPlayer->getAttackers().empty())
        {
            pPlayer->CLOSE_GOSSIP_MENU();
            pCreature->MonsterWhisper("No mientras combates, $N!",pPlayer->GetGUID());
        }
        return true;
    }
};

void AddSC_vip_vendor()
{
    new vip_vendor();
}