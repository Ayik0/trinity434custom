/*
 * Copyright (C) 2013-2014 Sovak <sovak007@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef END_TIME_H_
#define END_TIME_H_

#include "Map.h"
#include "CreatureAI.h"

enum DataTypes
{
    DATA_ECHO_OF_BAINE    = 0,
    DATA_ECHO_OF_JAINA    = 1,
    DATA_ECHO_OF_SYLVANAS = 2,
    DATA_ECHO_OF_TYRANDE  = 3,
    DATA_MUROZOND         = 4,
    MAX_ENCOUNTER         = 5,
    DATA_GHOUL1           = 4,
    DATA_GHOUL2           = 5,
    DATA_GHOUL3           = 6,
    DATA_GHOUL4           = 7,
    DATA_GHOUL5           = 8,
    DATA_GHOUL6           = 9,
    DATA_GHOUL7           = 10,
    DATA_GHOUL8           = 11,
    DATA_HOURGLASS        = 12,
    DATA_FRAGMENTS        = 13,
    DATA_BOSS1            = 14,
    DATA_BOSS2            = 15
};

enum Creatures
{
    BOSS_ECHO_OF_JAINA = 54445
};

enum Worldstates
{
    WORLDSTATE_SHOW_FRAGMENTS       = 6046,
    WORLDSTATE_FRAGMENTS_COLLECTED  = 6025,
};

template<class AI>
CreatureAI* GetEndTimeAI(Creature* creature)
{
    if (InstanceMap* instance = creature->GetMap()->ToInstanceMap())
        if (instance->GetInstanceScript())
            if (instance->GetScriptId() == sObjectMgr->GetScriptId("instance_end_time"))
                return new AI(creature);
    return NULL;
}

#endif
